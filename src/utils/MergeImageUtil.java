package utils;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;

import utils.MergeImageConfig.CoordinateType;

/**
 * 图片处理工具类
 * @author yzChen 2016-8-25 11:39:50
 */
public class MergeImageUtil {
	
	/**
	 * 合成图片
	 * @param originalFileName		原图文件
	 * @param illustrationFileName	插图文件
	 * @param config				合并图片配置
	 * @return
	 */
    public static File mergeImg(String originalFileName, String illustrationFileName, MergeImageConfig config) {
    	InputStream originalStream = null;
    	InputStream illustrationStream = null;
    	// 读取原图、插图文件流
    	try {
	        originalStream = new FileInputStream(originalFileName);
	        illustrationStream = new FileInputStream(illustrationFileName);
	        
	    	return mergeImg(originalStream, illustrationStream, config);
    	 } catch (Exception e) {
             e.printStackTrace();
         } finally {
         	// 关闭文件流
         	if(null != originalStream) {
 				try {originalStream.close();} catch (IOException e) {e.printStackTrace();}
         	}
         	if(null != illustrationStream) {
 				try {illustrationStream.close();} catch (IOException e) {e.printStackTrace();}
         	}
         }
    	return null;
    }
    
    /**
	 * 合成图片
	 * @param originalFileName		原图文件
	 * @param illustrationFileName	插图文件
	 * @param config				合并图片配置
	 * @return
	 */
    public static File mergeImg(InputStream originalStream, InputStream illustrationStream, MergeImageConfig config) {
    	File file = null;
    	OutputStream outImage= null;
        try {
        	// 创建文件
        	if(null != config.getDirectory() && !"".equals(config.getDirectory())) {
            	file = File.createTempFile(config.getFileName(), config.getFileSuffix(),
            			new File(config.getDirectory()));
        	} else {
        		file = File.createTempFile(config.getFileName(), config.getFileSuffix());
        	}
        	
            BufferedImage originalBuffered = ImageIO.read(originalStream);
            BufferedImage illustrationBuffered =ImageIO.read(illustrationStream);
            
            // 获取插图坐标
            int x = 0;
            int y = 0;
            // 原图宽度、高度
            int originalWidth = originalBuffered.getWidth();
            int originalHeight = originalBuffered.getHeight();
            // 插图宽度、高度
            int illustrationWidth = illustrationBuffered.getWidth();
            int illustrationHeight = illustrationBuffered.getHeight();
            // 偏移像素
            int marginX = config.getMarginX();
            int marginY = config.getMarginY();
            
            if(CoordinateType.TOP_LEFT == config.getCoordinateType()) {
            	x = marginX;
            	y = marginY;
            } else if(CoordinateType.TOP_RIGHT == config.getCoordinateType()) {
            	x = originalWidth - illustrationWidth - marginX;
            	y = marginY;
            } else if(CoordinateType.BOTTOM_LEFT == config.getCoordinateType()) {
            	x = marginX;
            	y = originalHeight - illustrationHeight - marginY;
            } else if(CoordinateType.BOTTOM_RIGHT == config.getCoordinateType()) {
            	x = originalWidth - illustrationWidth - marginX;
            	y = originalHeight - illustrationHeight - marginY;
            }
            
            // 开始实现插图
            Graphics g = originalBuffered.getGraphics();
            g.drawImage(illustrationBuffered, x, y, null);
            
            // 写入插图后的文件流到文件
            outImage = new FileOutputStream(file);
            ImageIO.write(originalBuffered, "jpeg", outImage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	// 关闭文件流
        	if(null != outImage) {
        		try {outImage.close();} catch (IOException e) {e.printStackTrace();}
        	}
        }
        
        return file;
    }
    
    /**
     * 合成文本，默认  微软雅黑、加粗、20像素大小
     * @param originalFileName
     * @param text
     * @param config
     * @return    
     * @author yzChen  cyzshenzhen@163.com
     * @date 2016年8月26日 上午10:31:19
     */
    public static File mergeText(String originalFileName,String text, MergeImageConfig config) {
    	Font font = new Font("微软雅黑", Font.BOLD, 20);
    	return mergeText(originalFileName, text, font, config);
    }
    
    /**
     * 合成文本，默认  微软雅黑、加粗、20像素大小
     * @param originalStream
     * @param text
     * @param config
     * @return    
     * @author yzChen  cyzshenzhen@163.com
     * @date 2016年8月26日 上午10:32:24
     */
    public static File mergeText(InputStream originalStream, String text, MergeImageConfig config) {
    	Font font = new Font("微软雅黑", Font.BOLD, 20);
    	return mergeText(originalStream, text, font, config);
    }
	
    /**
     * 插入文本
     * @param originalFileName
     * @param text
     * @param font
     * @param config
     * @return    
     * @author yzChen  cyzshenzhen@163.com
     * @date 2016年8月26日 上午10:30:01
     */
	public static File mergeText(String originalFileName,String text, Font font, MergeImageConfig config) {
		InputStream originalStream = null;
    	InputStream illustrationStream = null;
    	// 读取原图、插图文件流
    	try {
	        originalStream = new FileInputStream(originalFileName);
	        
	    	return mergeText(originalStream, text, font, config);
    	 } catch (Exception e) {
             e.printStackTrace();
         } finally {
         	// 关闭文件流
         	if(null != originalStream) {
 				try {originalStream.close();} catch (IOException e) {e.printStackTrace();}
         	}
         	if(null != illustrationStream) {
 				try {illustrationStream.close();} catch (IOException e) {e.printStackTrace();}
         	}
         }
		
		return null;
	}
    
    /**
	 * 合成文本
	 * @param originalFileName		原图文件
	 * @param illustrationFileName	插图文件
	 * @param config				合并图片配置
	 * @return
	 */
    public static File mergeText(InputStream originalStream, String text, Font font, MergeImageConfig config) {
    	File file = null;
    	OutputStream outImage= null;
        try {
        	// 创建文件
        	if(null != config.getDirectory() && !"".equals(config.getDirectory())) {
            	file = File.createTempFile(config.getFileName(), config.getFileSuffix(),
            			new File(config.getDirectory()));
        	} else {
        		file = File.createTempFile(config.getFileName(), config.getFileSuffix());
        	}
        	
            BufferedImage originalBuffered = ImageIO.read(originalStream);
            
            // 获取插图坐标
            int x = 0;
            int y = 0;
            // 原图宽度、高度
            int originalWidth = originalBuffered.getWidth();
            int originalHeight = originalBuffered.getHeight();
            // 获取文本所占像素，获取插入文本的宽度、高度
            int textUsePx = FontUtil.textUsePx(text, originalWidth, font);
            int illustrationWidth = textUsePx;
            int illustrationHeight = font.getSize();
            // 偏移像素
            int marginX = config.getMarginX();
            int marginY = config.getMarginY();
            
            if(CoordinateType.TOP_LEFT == config.getCoordinateType()) {
            	x = marginX;
            	y = marginY;
            } else if(CoordinateType.TOP_RIGHT == config.getCoordinateType()) {
            	x = originalWidth - illustrationWidth - marginX;
            	y = marginY;
            } else if(CoordinateType.BOTTOM_LEFT == config.getCoordinateType()) {
            	x = marginX;
            	y = originalHeight - illustrationHeight - marginY;
            } else if(CoordinateType.BOTTOM_RIGHT == config.getCoordinateType()) {
            	x = originalWidth - illustrationWidth - marginX;
            	y = originalHeight - illustrationHeight - marginY;
            }
            
            // 开始实现插图
            Graphics g = originalBuffered.getGraphics();
            g.setFont(font);
            g.drawString(text, x, y);
            
            // 写入插图后的文件流到文件
            outImage = new FileOutputStream(file);
            ImageIO.write(originalBuffered, "jpeg", outImage);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        	// 关闭文件流
        	if(null != outImage) {
        		try {outImage.close();} catch (IOException e) {e.printStackTrace();}
        	}
        }
        
        return file;
    }
    
}

