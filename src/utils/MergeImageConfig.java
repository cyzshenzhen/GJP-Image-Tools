package utils;

import java.io.Serializable;
import java.util.Date;

/**
 * 合并参数配置
 * @author yzChen 2016-8-25 10:25:08
 */
public class MergeImageConfig implements Serializable {
	
	private static final long serialVersionUID = 144092595832500675L;
	
	private String fileName = String.valueOf(new Date().getTime());	// 文件名称，默认当前时间戳
	private String fileSuffix = ".jpg";								// 文件后缀，默认 jpg 后缀
	private String directory;
	private CoordinateType coordinateType = CoordinateType.TOP_LEFT;// 插图基于角度类型，默认基于左上角
	private int marginX = 0;											// 插图横向距离像素
	private int marginY = 0;											// 插图竖向距离像素

	public String getFileName() {
		return fileName;
	}

	public String getFileSuffix() {
		return fileSuffix;
	}

	public String getDirectory() {
		return directory;
	}

	public CoordinateType getCoordinateType() {
		return coordinateType;
	}

	public int getMarginX() {
		return marginX;
	}

	public int getMarginY() {
		return marginY;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFileSuffix(String fileSuffix) {
		this.fileSuffix = fileSuffix;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public void setCoordinateType(CoordinateType coordinateType) {
		this.coordinateType = coordinateType;
	}

	public void setMarginX(int marginX) {
		this.marginX = marginX;
	}

	public void setMarginY(int marginY) {
		this.marginY = marginY;
	}

	
	/**
	 * 坐标类型
	 * @author yzChen 2016-8-25 11:13:03
	 */
	public enum CoordinateType {
		TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT;
	}

}
