package utils;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Toolkit;

/** 
 * 字体工具类
 * @author yzChen
 * @date 2016年8月26日 上午10:16:07 
 */
public class FontUtil {
	
	// 默认字符大小
	private static final String defaultFontName = "Arial";
	private static final Font defaultEnglishFont = new Font(defaultFontName, Font.BOLD, 20);
	@SuppressWarnings("deprecation")
	private static final FontMetrics defaultEnglishMetrics = Toolkit.getDefaultToolkit().getFontMetrics(defaultEnglishFont);
	// 中文字符大小
	private static final Font defaultChineseFont = new Font("微软雅黑", Font.BOLD, 20);
	@SuppressWarnings("deprecation")
	private static final FontMetrics defaultChineseMetrics = Toolkit.getDefaultToolkit().getFontMetrics(defaultChineseFont);

	/**
	 * 获取文本所占用的像素宽度
	 * @param strValue	文本
	 * @param MaxLenth	最大宽度
	 * @param chineseFont	中文字符样式
	 * @return    像素值
	 * @author yzChen  cyzshenzhen@163.com
	 * @date 2016年8月26日 上午10:43:28
	 */
	@SuppressWarnings("deprecation")
	public static int textUsePx(String strValue, int MaxLenth, Font chineseFont) {
		// 获取文本样式信息及配置
		FontMetrics englishMetrics, chineseMetrics;
		if(null == chineseFont) {
			englishMetrics = defaultEnglishMetrics;
			chineseMetrics = defaultChineseMetrics;
		} else {
			englishMetrics = Toolkit.getDefaultToolkit().getFontMetrics(
					new Font(defaultFontName, chineseFont.getStyle(), chineseFont.getSize()));
			chineseMetrics = Toolkit.getDefaultToolkit().getFontMetrics(chineseFont);
		}
		
		
		String tmpChar = "";
		int w_width = chineseMetrics.stringWidth("W");
		int chr_width = 0;
		int totalWidth = 0;
		if (strValue != null) {
			for (int i = 0; i < strValue.length(); i++) {
				tmpChar = strValue.substring(i, i + 1);
				if (tmpChar.getBytes().length == 1) {	// 英文数字等
					chr_width = englishMetrics.stringWidth(tmpChar);
				} else {	// 全角文字
					chr_width = chineseMetrics.stringWidth(tmpChar);
				}
				// 总宽度大于指定的宽度后，截取终了。
				if ((totalWidth + chr_width) > w_width * MaxLenth) {
					break;
				}
				totalWidth += chr_width;
			}
		}
		return totalWidth;
	}
}
