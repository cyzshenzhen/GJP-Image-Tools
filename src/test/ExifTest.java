package test;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Iterator;

import javax.imageio.ImageIO;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;

public class ExifTest {
	
	public static void printMetadata(Metadata metadata) {
		// 获取所有信息，包含 文件名称、文件大小、来源信息、图像信息、照相机信息等
		for (Directory exif : metadata.getDirectories()) {
			System.out.println("信息封装类 -> " + exif.getClass());
			Iterator tags = exif.getTags().iterator();
			while (tags.hasNext()) {
				Tag tag = (Tag) tags.next();
				System.out.println(tag);
			}
		}
	}

	public static void main(String[] args) throws Exception {
		String fileName = "E:/a.jpg";
		String destFileName = "E:/test2.jpg";
		
		rotateImage(fileName, destFileName);
	}
	
	/**
	 * 根据照片属性，自动旋转图片矫正照片方向
	 * @param fileName
	 * @param destFileName
	 * @throws Exception    
	 * @author yzChen
	 * @date 2016年9月25日 下午5:15:28
	 */
	public static void rotateImage(String fileName, String destFileName) throws Exception {
		File jpegFile = new File(fileName);
		
		// 获取照片的属性信息
		Metadata metadata = JpegMetadataReader.readMetadata(jpegFile);

		// 打印信息
		// printMetadata(metadata);
		
		// 获取照片宽度、高度
		BufferedImage old_img = (BufferedImage) ImageIO.read(jpegFile);
		int width = old_img.getWidth();
		int height = old_img.getHeight();
		
		// 旋转后宽度、高度
		int newWidth = 0;
		int newHeight = 0;
		// 旋转角度、横向定位、纵向定位
		double theta = 0D;
		double anchorX = 0D;
		double anchorY = 0D;

		// 获取照片高级信息封装类
		Directory exif = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
		// 获取照片拍摄角度信息
		int orientation = exif.getInt(ExifIFD0Directory.TAG_ORIENTATION);
		// 根据 orientation 获取文字描述，即 顺时针/逆时针 旋转 x 度
		String description = exif.getDescription(ExifIFD0Directory.TAG_ORIENTATION);

		// 根据 orientation 值，自动计算旋转信息
		switch (orientation) {
			case 1: break;
			case 2: break;
			case 3: break;
			case 4: break;
			case 5: break;
			case 6: newWidth = height; newHeight = width; theta = 90D; anchorX = (width - height) * 1.5; anchorY = height / 2; break;
			case 7: break;
			case 8: break;
			default: break;
		}

		// 定义新图片对象
		BufferedImage newImg = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_BGR);

		// 旋转角度等参数设置
		Graphics2D g2d = newImg.createGraphics();
		AffineTransform origXform = g2d.getTransform();
		AffineTransform newXform = (AffineTransform) (origXform.clone());
		newXform.rotate(Math.toRadians(theta), anchorX, anchorY);
		// 执行旋转操作
		g2d.setTransform(newXform);
		g2d.drawImage(old_img, 0, 0, null);
		g2d.setTransform(origXform);

		//写到新的文件  
		FileOutputStream out = new FileOutputStream(destFileName);
		try {
			ImageIO.write(newImg, "JPG", out);
		} finally {
			out.close();
		}
	}

}
