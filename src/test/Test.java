package test;

import utils.MergeImageUtil;
import utils.MergeImageConfig;


public class Test {

	public static void main(String[] args) {
//		String fileName1 = "E:/t.jpg";
		String fileName1 = "E:/a.jpg";
		String fileName2 = "E:/git.png";
		
		MergeImageConfig config = new MergeImageConfig();
		config.setDirectory("E:");
		config.setCoordinateType(MergeImageConfig.CoordinateType.BOTTOM_RIGHT);
		config.setMarginX(20);
		config.setMarginY(20);
		
		MergeImageUtil.mergeImg(fileName1, fileName2, config);
		
//		ImageUtils.mergeText(fileName1, "随便写几个字", config);
	}
	
}
