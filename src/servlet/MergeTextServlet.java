package servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import utils.MergeImageUtil;
import utils.MergeImageConfig;
import utils.MergeImageConfig.CoordinateType;

public class MergeTextServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MergeTextServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InputStream originalStream = null;
		String str = "的放跌改革";
		
		try {
			//使用Apache文件上传组件处理文件上传步骤：
			//1、创建一个DiskFileItemFactory工厂
			DiskFileItemFactory factory = new DiskFileItemFactory();
			//2、创建一个文件上传解析器
			ServletFileUpload upload = new ServletFileUpload(factory);
			//解决上传文件名的中文乱码
			upload.setHeaderEncoding("UTF-8");
			//3、判断提交上来的数据是否是上传表单的数据
			if (!ServletFileUpload.isMultipartContent(request)) {
				//按照传统方式获取数据
				return;
			}
			//4、使用ServletFileUpload解析器解析上传数据，解析结果返回的是一个List<FileItem>集合，每一个FileItem对应一个Form表单的输入项
			List<FileItem> list = upload.parseRequest(request);
			for (FileItem item : list) {
				//如果fileitem中封装的是普通输入项的数据
				if (!item.isFormField()) {
					//如果fileitem中封装的是上传文件
					//得到上传的文件名称，
					String fieldName = item.getFieldName();
					String filename = item.getName();
					if (filename == null || filename.trim().equals("")) {
						continue;
					}
					//注意：不同的浏览器提交的文件名是不一样的，有些浏览器提交上来的文件名是带有路径的，如：  c:\a\b\1.txt，而有些只是单纯的文件名，如：1.txt
					//处理获取到的上传文件的文件名的路径部分，只保留文件名部分
					//获取item中的上传文件的输入流
					InputStream in = item.getInputStream();
					if("original".equals(fieldName)) {
						originalStream = in;
					} else {
						//关闭输入流
						in.close();
					}
					//删除处理文件上传时生成的临时文件
					item.delete();
				}
			}
			
			// 插图
			MergeImageConfig config = new MergeImageConfig();
			config.setDirectory(request.getSession().getServletContext().getRealPath("/")+"upload/");
			config.setCoordinateType(CoordinateType.BOTTOM_RIGHT);
			config.setMarginX(20);
			config.setMarginY(20);
			
			// 插图操作
			File outFile = MergeImageUtil.mergeText(originalStream, str, config);
			
			// 返回图片地址
			request.setAttribute("image", "upload/" + outFile.getName());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

}
