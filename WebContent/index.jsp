<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<link rel="stylesheet" type="text/css" href="styles/weui.css" />

<style type="text/css">
body{background-color: #fbf9fe;}
.container{height: 100%;overflow-y: auto;}
.spacing{padding: 0 15px;}
.spacing div{padding: 10px;}
input{height:80px;}
.upload{position: relative;}
.upload_file{opacity: 0;cursor: pointer;position: absolute; top: -20px;}
</style>

</head>
<body ontouchstart>
<div class="container">
	<div class="spacing">
		<form action="MergeImageServlet" method="post" enctype="multipart/form-data">
			<div class="upload">
				<input class="weui_btn weui_btn_plain_primary" type="button" value="选择原图" id="originalSelectBtn"/>
				<input class="weui_btn upload_file" type="file" id="original" name="original">
			</div>
			<div class="upload">
				<input class="weui_btn weui_btn_plain_primary" type="button" value="选择水印" id="illustrationSelectBtn"/>
				<input class="weui_btn upload_file" type="file" id="illustration" name="illustration">
			</div>
			<div>
				<input class="weui_btn weui_btn_primary" type="submit" value="嵌入水印"/>
			</div>
			<div>
			<img alt="" src="${image}">
			</div>
		</form>
	</div>
</div>


<!-- 
<form action="MergeTextServlet" method="post" enctype="multipart/form-data">
	<input type="file" id="original" name="original">
	<br/>
	<br/>
	<input type="submit" />
	<br/>
	<img alt="" src="${image}">
</form>
-->

<script type="text/javascript" src="javascripts/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
	$('#original').change(function() {
		$('#originalSelectBtn').val('原图已选择');
	});

	$('#illustration').change(function() {
		$('#illustrationSelectBtn').val('水印已选择');
	});
});
</script>
</body>
</html>